package az.atl.product.service.impl;

import az.atl.product.dao.entity.ProductEntity;
import az.atl.product.dao.repository.ProductRepo;
import az.atl.product.model.dto.ProductDto;
import az.atl.product.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

import static az.atl.product.model.mapper.ProductMapper.buildDtoList;

@Service
@AllArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService, CommandLineRunner {

    private final ProductRepo productRepo;

    @Override
    public List<ProductDto> getAllProducts() {
        log.warn("getAllProducts.Start");
        var productList = productRepo.findAll();
        log.warn("getAllProducts.End");
        return buildDtoList(productList);
    }

    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < 5; i++) {
            var product = ProductEntity.builder()
                    .name("Product" + i)
                    .price(new BigDecimal(10).add(new BigDecimal(i)))
                    .count(i).build();
            productRepo.save(product);
        }
    }
}
